# gitpractice

git 과제를 위한 프로젝트

_NoChange폴더에 있는 소스는 수정하지 마세요._

_아래 설명한 브랜치에서 NoChange 폴더에 있는 소스를 사용하시면 됩니다._

### 상황 요약

_모든 브랜치는 다 생성되어 있습니다._

> 작업자 Shiva

- branch "1-feature_Shiva" 에서 WorkSpace폴더에 StopSliding.html, HideShow.html (HideShowShiva.html) 을 작업합니다.

> 작업자 Cammi

- branch "2-feature_Cammi" 에서 WorkSpace폴더에 Chaining.html, HideShow.html (HideShowCammi.html) 을 작업합니다.

### 해야할 일

_반드시 HideShowShiva, HideShowCammi --> HideShow.html 파일로 이름 변경해서 사용하세요._

> 작업자 Shiva

- branch "1-feature_Shiva" 에서 WorkSpace폴더에 StopSliding.html 추가하는 작업 진행합니다.

- branch "1-feature_Shiva" 에서 HideShow.html 코드 리뷰에 내용을 수정 작업 진행합니다.

> 작업자 Cammi

- branch "2-feature_Cammi" 에서 WorkSpace폴더에 Chaining.html, HideShow.html 추가하는 작업 진행합니다.

> 관리자 Bigone (분기 작업 후 진행 할 것)

- 배포를 위해 master 분기에 "1-feature_Shiva", "2-feature_Cammi" 분기를 병합 처리 작업을 진행합니다.

- 병합 충돌 시 (HideShow.html) "1-feature_Shiva" 파일을 기준으로 충돌을 해결합니다.
